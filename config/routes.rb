# frozen_string_literal: true

Rails.application.routes.draw do

  require "sidekiq/web"

  authenticate :admin do
    mount Sidekiq::Web => "/sidekiq"
  end

  default_url_options host: ENV['DEFAULT_URL']

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  post '/graphql', to: 'graphql#execute'
  devise_for :users,
             controllers: {
               confirmations: 'auth/confirmations',
               passwords: 'auth/passwords',
               invitations: 'auth/invitations'
             },
             skip: :registrations # skip registration route

  # Just a blank root path
  root 'pages#blank'
end
