user = User.create(
  email: Settings.initial_admin.email,
  password: Settings.initial_admin.password,
  password_confirmation: Settings.initial_admin.password,
  first_name: Settings.initial_admin.first_name,
  last_name: Settings.initial_admin.last_name,
  role: 'superadmin'
)

if user
  Rails.logger.info "Login with #{ENV['ADMIN_EMAIL']} and #{Settings.initial_admin.password}"
end

# Create a admin user
admin = User.create(
  email: Faker::Internet.email,
  password: Settings.initial_admin.password,
  password_confirmation: Settings.initial_admin.password,
  first_name: Faker::Name.first_name,
  last_name: Faker::Name.last_name,
  role: 'admin'
)

User.create(
  email: Faker::Internet.email,
  password: Settings.initial_admin.password,
  password_confirmation: Settings.initial_admin.password,
  first_name: Faker::Name.first_name,
  last_name: Faker::Name.last_name,
  role: 'user',
  company_id: admin.company_id
)

User.create(
  email: Faker::Internet.email,
  password: Settings.initial_admin.password,
  password_confirmation: Settings.initial_admin.password,
  first_name: Faker::Name.first_name,
  last_name: Faker::Name.last_name,
  role: 'user',
  company_id: admin.company_id
)
