## Troviet API project

[![pipeline status](https://gitlab.com/thachnguyen/tv_be/badges/master/pipeline.svg)](https://gitlab.com/thachnguyen/tv_be/-/commits/master)
[![coverage report](https://gitlab.com/thachnguyen/tv_be/badges/master/coverage.svg)](https://gitlab.com/thachnguyen/tv_be/-/commits/master)

### Poppose
 - Help us :
   - Build dockerfile
   - How to integrate CI/CI and deploy it to Prod
### Docker

#### Setup
Go to https://github.com/settings/tokens and generate token in 'Personal access tokens' to get git_token_generate.
- In-case that you don't have `.env` file:
```
  cp .env_example .env
```
And update your env variables

- Build docker containers:
```
docker-compose build
docker-compose up
docker-compose run troviet_api ./bin/rails db:create
docker-compose run troviet_api ./bin/rails db:migrate
docker-compose run troviet_api ./bin/rails db:import_master_data
docker-compose run -e RAILS_ENV=test troviet_api ./bin/rails db:migrate
docker-compose restart troviet_api
```

#### Start Application
```
docker-compose up
```