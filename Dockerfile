#-----------------------------------------#
# Common to development/staging/production
#-----------------------------------------#
FROM ruby:2.6.6-slim-stretch

ENV APP_DIR /app
ENV ENCRYPTION_KEY="NIZ0G6NI7BouauDWuvur30rGbnuOb0WooIiTYCxR"
ENV SECRET_KEY_BASE="omGKXXzT1pCe9BtAHbrT1fS2KjjHd79uXLd0WbWX"

RUN mkdir -p $APP_DIR
WORKDIR $APP_DIR

RUN mkdir -p /usr/share/man/man1 \
    && mkdir -p /usr/share/man/man7

RUN apt-get update -qq \
    && apt-get install -y --no-install-recommends git build-essential libpq-dev postgresql-client \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean \
   ;

# Copy application files and install the bundle
COPY Gemfile* ./
#It suppresses the documentation generation when installing each new gem.
RUN echo "gem: --no-rdoc --no-ri" > ~/.gemrc
RUN gem install bundler:2.1.4
RUN bundle install -j4
COPY . .

# Run asset pipeline. - Temp off
#RUN ENCRYPTION_KEY=${ENCRYPTION_KEY} SECRET_KEY_BASE=${SECRET_KEY_BASE} bin/rails assets:precompile RAILS_ENV=production

# Remove folders not needed in resulting image
#RUN rm -rf node_modules tmp/cache app/assets spec
#RUN rm -rf ~/.bundle/cache && \
#    rm -rf /usr/local/bundle/cache/*.gem && \
#    rm -rf /usr/local/bundle/cache/bundler

#Run make sure pid port has been deleted
ENTRYPOINT ["./entrypoint.sh"]
EXPOSE 8080
CMD ["bundle", "exec", "rackup", "--port=8080", "--env=production"]